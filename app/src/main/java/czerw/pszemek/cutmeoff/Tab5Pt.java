package czerw.pszemek.cutmeoff;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by Pszemek on 2017-09-05.
 */

public class Tab5Pt extends Fragment {

    public Tab5Pt(){

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.tab_5_pt, container, false);
        return rootView;
    }
}
