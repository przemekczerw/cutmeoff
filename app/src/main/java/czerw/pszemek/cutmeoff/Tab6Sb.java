package czerw.pszemek.cutmeoff;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


/* Created by Pszemek on 2017-09-05.
        */

public class Tab6Sb extends Fragment {


    public Tab6Sb() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.tab_6_sb, container, false);
        return rootView;
    }

}