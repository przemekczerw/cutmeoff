package czerw.pszemek.cutmeoff;

/**
 * Created by Pszemek on 2017-09-05.
 */

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by Pszemek on 2017-09-05.
 */

public class Tab7Nd extends Fragment {

    public Tab7Nd(){

    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.tab_7_nd, container, false);
        return rootView;
    }
}

