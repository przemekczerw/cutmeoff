package czerw.pszemek.cutmeoff.Dane;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import czerw.pszemek.cutmeoff.Tab1Pon;
import czerw.pszemek.cutmeoff.Tab2Wt;
import czerw.pszemek.cutmeoff.Tab3Sr;
import czerw.pszemek.cutmeoff.Tab4Czw;
import czerw.pszemek.cutmeoff.Tab5Pt;
import czerw.pszemek.cutmeoff.Tab6Sb;
import czerw.pszemek.cutmeoff.Tab7Nd;

/**
 * Created by Pszemek on 2017-09-05.
 */

public class TabsAdapter extends FragmentStatePagerAdapter{

        int mNumOfTabs;


        public TabsAdapter(FragmentManager fm, int NumOfTabs) {
            super(fm);
            this.mNumOfTabs = NumOfTabs;
        }

        @Override
        public Fragment getItem(int position) {

            switch (position) {
                case 0:
                    return new Tab1Pon();
                case 1:
                    return new Tab2Wt();
                case 2:
                    return new Tab3Sr();
                case 3:
                    return new Tab4Czw();
                case 4:
                    return new Tab5Pt();
                case 5:
                    return new Tab6Sb();
                case 6:
                    return new Tab7Nd();
                default:
                    return null;
            }
        }

        @Override
        public int getCount() {
            return mNumOfTabs;
        }
    }

