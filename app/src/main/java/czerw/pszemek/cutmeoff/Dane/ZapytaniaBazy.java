package czerw.pszemek.cutmeoff.Dane;

/**
 * Created by przemyslawczerw on 14.07.2017.
 */

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;


public class ZapytaniaBazy {

    private SQLiteDatabase baza;
    private PomocnikBazy pomocnikBazy;

    public ZapytaniaBazy(Context context) {
        pomocnikBazy = new PomocnikBazy(context);
    }

    public void otworzZapis() throws SQLException {
        baza = pomocnikBazy.getWritableDatabase();
    }

    public void otworzOdczyt() {
        baza = pomocnikBazy.getReadableDatabase();
    }

    public void zamknij() {
        baza.close();
    }


    public void czyscielBazy() {
        baza.execSQL("DELETE FROM " + ToDoContract.ToDoTabela.NAZWA_TABELI);

        // korzystamy z  baza.rawQuery(); wtedy kiedy chcemu uzyc SELECT bo on zwraca Cursor
    }


    public List<ToDoModel> zwrocListeToDo() {
        ArrayList<ToDoModel> listaZrobic = new ArrayList<ToDoModel>();

        Cursor cursor = baza.query(ToDoContract.ToDoTabela.NAZWA_TABELI, null, null, null, null, null, null);


        ToDoModel toDoModel;

        // sprawdzamy czy jest cos na liscie
        if (cursor.getCount() > 0) {
            // kursor przechodzi po kazdym elemncie listy
            for (int i = 0; i < cursor.getCount(); i++) {
                cursor.moveToNext();
                toDoModel = new ToDoModel();
                toDoModel.setId(cursor.getInt(0));
                toDoModel.setNazwaZadania(cursor.getString(1));
                toDoModel.setZrobione(cursor.getInt(2));
                listaZrobic.add(toDoModel);

            }
        }

        return listaZrobic;
    }


    public String dodajToDoBazy(String nazwaZadania, int zrobione) {
        ContentValues wartosci = new ContentValues();
        wartosci.put(ToDoContract.ToDoTabela.KOLUMNA_NAZWA_ZADANIA, nazwaZadania);
        wartosci.put(ToDoContract.ToDoTabela.KOLUMNA_CZY_ZROBIONE, zrobione);

        long numerWiersza = baza.insert(ToDoContract.ToDoTabela.NAZWA_TABELI, null, wartosci);

        return String.valueOf(numerWiersza);
    }

    public void usunWierszToDo(String id) {
        String selection = ToDoContract.ToDoTabela._ID + " = ?";
        String selectionArgumenty[] = {id};
        // powyzsze rowna sie temu: np.  WHERE id = ? zamienia na WHERE id = 1;
        baza.delete(ToDoContract.ToDoTabela.NAZWA_TABELI, selection, selectionArgumenty);

    }

    public void edytujWierszToDo(String nazwa, String id) {
        // ustalamy jaka kolumne chcemy zmienic i podajemy wartosc na jaka zmieniamy
        ContentValues wartosci = new ContentValues();
        wartosci.put(ToDoContract.ToDoTabela.KOLUMNA_NAZWA_ZADANIA, nazwa);


        String selection = ToDoContract.ToDoTabela._ID + " = ?";
        String selectionArgumenty[] = {id};

        baza.update(ToDoContract.ToDoTabela.NAZWA_TABELI, wartosci, selection, selectionArgumenty);

    }

}

