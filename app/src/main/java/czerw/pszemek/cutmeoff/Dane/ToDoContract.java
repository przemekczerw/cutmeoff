package czerw.pszemek.cutmeoff.Dane;

/**
 * Created by przemyslawczerw on 14.07.2017.
 */

import android.provider.BaseColumns;

public final class ToDoContract {

    private ToDoContract() {
    }

    // opisujemy jak wygladaja tabele i kolumny
    public static class ToDoTabela implements BaseColumns {
        public static final String NAZWA_TABELI = "ToDo";
        public static final String KOLUMNA_NAZWA_ZADANIA = "zadanie";
        public static final String KOLUMNA_NAZWA_SZCZEGOLY = "szczegoly";
        public static final String KOLUMNA_CZY_ZROBIONE = "zrobione";
        // psfs + enter (tajny kod seniora)
    }


}