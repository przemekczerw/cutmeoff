package czerw.pszemek.cutmeoff.Dane;

/**
 * Created by przemyslawczerw on 14.07.2017.
 */

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;


// sluzy lacznie z baza danych nasz pomocnik


public class PomocnikBazy extends SQLiteOpenHelper {

    public static final int DATABASE_VERSION = 2;
    public static final String DATABASE_NAME = "BazaToDoListy.db";




    /**
     * To jest rowne zapytania: CREATE TABLE FilmModel (film_id integer primary key, tytul text, budzet integer, rokPowstania text);
     */

    private static final String SQL_UTWORZ_TABELE_TODO = "CREATE TABLE " + ToDoContract.ToDoTabela.NAZWA_TABELI +
            " (" + ToDoContract.ToDoTabela._ID + " INTEGER PRIMARY KEY," +
            ToDoContract.ToDoTabela.KOLUMNA_NAZWA_ZADANIA + " TEXT," +
            ToDoContract.ToDoTabela.KOLUMNA_NAZWA_SZCZEGOLY + " TEXT," +
            ToDoContract.ToDoTabela.KOLUMNA_CZY_ZROBIONE + " INTEGER)";



    public static final String SQL_SKASUJ_TABELE_TODO = "DROP TABLE IF EXISTS " + ToDoContract.ToDoTabela.NAZWA_TABELI;


    public PomocnikBazy(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL(SQL_UTWORZ_TABELE_TODO);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        db.execSQL(SQL_SKASUJ_TABELE_TODO);
        onCreate(db);
    }
}