package czerw.pszemek.cutmeoff;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


/**
 * Created by Pszemek on 2017-09-05.
 */

public class Tab2Wt  extends Fragment {

    public Tab2Wt() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.tab_2_wt, container, false);
        return rootView;
    }
}
