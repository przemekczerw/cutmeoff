package czerw.pszemek.cutmeoff;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.EditText;

import java.text.Format;
import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import czerw.pszemek.cutmeoff.databinding.ActivityRegistrationUserDataBinding;

public class RegistrationUserDataActivity extends AppCompatActivity {
    Calendar date;
    int todayDate;

    Format formatter;
    @BindView(R.id.userBirth)
    Button userBirthButton;
    @BindView(R.id.setUserBirth)
    EditText setUserBirth;
    @BindView(R.id.userBirthText)
    EditText userBirthText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration_user_data);
        ButterKnife.bind(this);

//        String imie = getIntent().getExtras().getString("RegImie");
//        RegImie.setText(imie);
//
//        String nazwisko = getIntent().getExtras().getString("RegNazwisko");
//        RegNazwisko.setText(nazwisko);

        ActivityRegistrationUserDataBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_registration_user_data);
        Uzytkownik nowyuzytkownik = (Uzytkownik) getIntent().getSerializableExtra("caly_Uzytkownik");
        binding.setUzytkownik(nowyuzytkownik);

//        formatter = new SimpleDateFormat("dd-MM-yyyy");

        date = Calendar.getInstance();
        todayDate = date.get(Calendar.YEAR);

    }

    @OnClick(R.id.userBirth)
    public void checkBirth() {

        todayDate -= Integer.valueOf(setUserBirth.getText().toString());
        userBirthText.setText(String.valueOf(todayDate));

    }




}
