package czerw.pszemek.cutmeoff;


import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import czerw.pszemek.cutmeoff.Dane.TabsAdapter;

public class TabsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tabs);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.addTab(tabLayout.newTab().setText("Pon"));
        tabLayout.addTab(tabLayout.newTab().setText("Wt"));
        tabLayout.addTab(tabLayout.newTab().setText("Sr"));
        tabLayout.addTab(tabLayout.newTab().setText("Czw"));
        tabLayout.addTab(tabLayout.newTab().setText("Pt"));
        tabLayout.addTab(tabLayout.newTab().setText("Sb"));
        tabLayout.addTab(tabLayout.newTab().setText("Nd"));

        final ViewPager viewPager = (ViewPager) findViewById(R.id.pager);
        final TabsAdapter adapter = new TabsAdapter
                (getSupportFragmentManager(), tabLayout.getTabCount());
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }
}